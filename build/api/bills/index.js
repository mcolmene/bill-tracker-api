'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getBills = getBills;
exports.addBill = addBill;

var _common = require('../../common');

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var handleSuccess = common.handleSuccess,
    handleError = common.handleError,
    validate = common.validate,
    codes = common.codes;
function getBills(req, res, billsCollection) {
    var resultArray = [];
    billsCollection.find().forEach(function (doc, err) {
        err ? handleError(res, err.message, "Failed to get bill", codes.server_error) : resultArray.push(doc);
    }, function () {
        var responseObj = {
            bills: resultArray
        };
        handleSuccess(res, responseObj, codes.success);
        console.log(resultArray);
    });
}
function addBill(app, billsCollection) {
    app.post('/bills', function (req, res) {
        var keys = Object.keys(req.body);
        var _req$body = req.body,
            item = _req$body.item,
            type = _req$body.type,
            amount = _req$body.amount,
            description = _req$body.description,
            date = _req$body.date,
            user = _req$body.user;


        var bill = {};
        var flag = validate(keys, req);
        var errorMessage = void 0;
        !flag ? bill = {
            item: item,
            type: type,
            amount: amount,
            description: description,
            date: date,
            user: user
        } : errorMessage = 'Invalid characters entered';
        bill.item && bill.type && bill.amount && bill.date && bill.user ? billsCollection.insertOne(bill, function (err, result) {
            err ? handleError(err, 'Unable to save to database', err.message, codes.server_error) : handleSuccess(res, { "response": codes.success }, codes.success);
        }) : handleError(res, 'Invalid bill input', errorMessage || "missing a field", codes.bad_request);
    });
}