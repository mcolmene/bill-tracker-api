'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addUser = addUser;
exports.getUser = getUser;

var _common = require('../../common');

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var handleSuccess = common.handleSuccess,
    handleError = common.handleError,
    validate = common.validate,
    codes = common.codes;
function addUser(req, userCollection) {
    app.post('/users', function (req, res) {
        var user = new User({
            username: req.body.username,
            password: req.body.password
        });
        user.username && user.password ? userCollection.insertOne(user, function (err, result) {
            if (err) return handleError(res, err.message, "Failed to add a user");else {
                console.log('saved to database');
                res.status(codes.success).json({ "response": codes.success });
            }
        }) : handleError(res, "Invalid user input", "Must provide a name and password", 400);
    });
}
function getUser(app, res, userCollection) {
    app.get('/users', function (req, res) {
        var resultArray = [];
        userCollection.find().forEach(function (doc, err) {
            console.log("getting users");
            err ? handleError(res, err.message, "Failed to get users", codes.server_error) : resultArray.push(doc);
        }, function () {
            responseObj = {
                users: resultArray
            };
            handleSuccess(res, responseObj, codes.success);
            console.log(resultArray);
        });
    });
}