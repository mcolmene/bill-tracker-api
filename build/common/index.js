"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.handleError = handleError;
exports.handleSuccess = handleSuccess;
exports.validate = validate;
var codes = exports.codes = {
    success: 200,
    bad_request: 400,
    server_error: 500
};

function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({ "error": message });
}
function handleSuccess(res, responseObject, code) {
    res.status(code || 200).json(responseObject);
}
function validate(keys, req) {
    for (var i = 0; i < keys.length; i++) {
        if (req.body[keys[i]].includes('<') || req.body[keys[i]].includes('>')) {
            return true;
        }
    }
    return false;
}