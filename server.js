require('dotenv').config();
let express = require("express");
let bodyParser = require("body-parser");
const mongo = require('mongodb').MongoClient;
const billsApi = require ('./src/api/bills/index.js');

let app = express();
app.use(bodyParser.json());

// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
let db;

// cached collections to use throughout the app
let userCollection;
let billsCollection;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
//and remove cacheing so we get the most recent comments
    res.setHeader('Cache-Control', 'no-cache');
    next();
});
// Connect to the database before starting the application server.
mongo.connect(process.env.MONGODB_URI, function (err, database) {
    if (err) {
        console.log(err);
        process.exit(1);
    }
    // Save database object from the callback for reuse.
    db = database;
    console.log("Database connection ready");

    userCollection = db.collection('users');
    billsCollection = db.collection('bills');

    // Initialize the app.
    let server = app.listen(process.env.PORT || 8080, function () {
        let port = server.address().port;
        console.log("App now running on port", port);
    });
});

app.get('/bills', (req, res) => {
    billsApi.getBills(res, billsCollection);
});
app.post('/bills', (req, res) => {
    billsApi.addBill(req, res, billsCollection);
});
//To prevent errors from Cross Origin Resource Sharing, we will set
//our headers to allow CORS with middleware like so:

/*const userSchema = new mongoose.Schema({
    username: String,
    password: String
});
const billSchema = new mongoose.Schema({
    item: String,
    type: String,
    amount: String,
    description: String,
    date: String,
    user: String
});*/
/*
const User = mongoose.model('User', userSchema);
const Bill = mongoose.model('Bill', billSchema);
*/

// Generic error handler used by all endpoints.

