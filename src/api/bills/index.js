import * as common from '../../common';

const { handleSuccess, handleError, validate, codes } = common;

export function getBills(res, billsCollection) {
    const resultArray = [];
    billsCollection.find().forEach(function (doc, err) {
        (err)
            ? handleError(res, err.message, "Failed to get bill", codes.server_error)
            : resultArray.push(doc);
    }, () => {
        const responseObj = {
            bills: resultArray
        };
        handleSuccess(res, responseObj, codes.success);
        console.log(resultArray);
    })
}
export function addBill(req, res, billsCollection) {
    const keys = Object.keys(req.body);
    const {item, type, amount, description, date, user} = req.body;

    let bill = {};
    let flag = validate(keys, req);
    let errorMessage;
    (!flag)
        ? bill = {
            item,
            type,
            amount,
            description,
            date,
            user
        }
        : errorMessage = 'Invalid characters entered';
    (bill.item && bill.type && bill.amount && bill.date && bill.user)
        ? (billsCollection.insertOne(bill, (err, result) => {
            (err)
                ? handleError(err, 'Unable to save to database', err.message, codes.server_error)
                : (
                    handleSuccess(res, {"response": codes.success}, codes.success)
                );
        }))
        : (handleError(res, 'Invalid bill input', errorMessage || "missing a field", codes.bad_request));
}