import * as common from '../../common';

const { handleSuccess, handleError, validate, codes } = common;

export function addUser(req, userCollection) {
    app.post('/users', (req, res) => {
        const user = new User({
            username: req.body.username,
            password: req.body.password
        });
        (user.username && user.password)
            ? (userCollection.insertOne(user, (err, result) => {
                    if (err)
                        return handleError(res, err.message, "Failed to add a user");
                    else {
                        console.log('saved to database');
                        res.status(codes.success).json({"response": codes.success});
                    }
                })
            )
            : handleError(res, "Invalid user input", "Must provide a name and password", 400);
    });
}
export function getUser(app, res, userCollection) {
    app.get('/users', (req, res) => {
        const resultArray = [];
        userCollection.find().forEach( (doc, err) => {
            console.log("getting users");
            (err)
                ? handleError(res, err.message, "Failed to get users", codes.server_error)
                : resultArray.push(doc);
        }, () => {
            responseObj = {
                users: resultArray
            };
            handleSuccess(res, responseObj, codes.success);
            console.log(resultArray);
        })
    });
}
