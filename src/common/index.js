export const codes = {
    success: 200,
    bad_request:400,
    server_error:500
};

export function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({"error": message});
}
export function handleSuccess(res, responseObject, code) {
    res.status(code || 200).json(responseObject);
}
export function validate(keys, req) {
    for(let i = 0; i < keys.length; i++){
        if(req.body[keys[i]].includes('<') || req.body[keys[i]].includes('>')) {
            return true;
        }
    }
    return false;
}